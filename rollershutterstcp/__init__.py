import socket


def add_parity(byte):
    byte &= 0b00011111

    if not (byte & 0b00000001) ^ ((byte & 0b00000010) >> 1):
        byte |= 0b00100000
    if not ((byte & 0b00000100) >> 2) ^ ((byte & 0b00001000) >> 3):
        byte |= 0b01000000
    if not ((byte & 0b00010000) >> 4):
        byte |= 0b10000000
    return byte


def build_message(shutter, command):
    msg = shutter
    msg |= command << 3
    return add_parity(msg)


class RollerShutters:

    def __init__(self, host=None, port=None):
        self.connected = False
        if host is not None and port is not None:
            try:
                self.connect(host, port)
                self.connected = True
            except:
                raise

    def close(self):
        if self.connected:
            try:
                self.sock.close()
                self.connected = False
            except:
                raise

    def is_connected(self):
        return self.connected

    def connect(self, host, port):
        """
        """
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        try:
            # Connect to server and send data

            self.sock.connect((host, port))
            self.sock.settimeout(2.0)

            self.connected = True
        except:
            raise

    def send_raw_message(self, raw_message):
        """ send raw message. sends raw_message as is """
        try:
            self.sock.sendall(raw_message)
        except:
            raise

    def move(self, shutter, cmd):
        try:
            self.send_raw_message(bytes([build_message(shutter, cmd)]))
            self.sock.recv(1024)

        except:
            raise
