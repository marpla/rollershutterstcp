import setuptools

setuptools.setup(
    name='rollershutterstcp-marpla',
    version='0.0.1',
    url='https://gitlab.com/marpla/rollershutterstcp',
    packages=['rollershutterstcp'],
    license='',
    author='Martin Plaas',
    author_email='martin.plaas@marpla.org',
    description=''
)
